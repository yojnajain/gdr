package in.yojnajain.jsonserialization;

import in.yojnajain.gdr.SubCategoryListActivity;
import in.yojnajain.gdr.Utility;
import in.yojnajain.model.Questions;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

public class FetchQuestions extends AsyncTask<String, Void, Void> {

	SubCategoryListActivity context;
	String email, category, subCategory;
	Questions[] question;

	public FetchQuestions(SubCategoryListActivity context, String category, String subCategory) {
		super();
		this.context = context;
		this.category = category;
		this.subCategory = subCategory;
		email = Utility.findEmail(context);
	}

	@Override
	protected Void doInBackground(String... params) {

		DefaultHttpClient defaultClient = new DefaultHttpClient();
		// Setup the get request
	
		HttpGet httpGetRequest = new HttpGet(
				"https://gdrwebsite.herokuapp.com/api/question/?category="
						+ URLEncoder.encode(category) + "&subCategory=" + URLEncoder.encode(subCategory) + "&email=" +email);
		Log.i("URL", httpGetRequest.getURI().toString());
		// Execute the request in the client
		HttpResponse httpResponse;
		try {
			httpResponse = defaultClient.execute(httpGetRequest);
			String s = EntityUtils.toString(httpResponse.getEntity());
			Log.i("String : ", s);
			Gson gson = new Gson();
			question = gson.fromJson(s, Questions[].class);
			Log.i("Array Size", "" + question.length);
			/*JsonToSQLiteDB jtsb = new JsonToSQLiteDB(context);

			for (int i = 0; i < question.length; i++) {
				Questions ques = question[i];
				jtsb.QuesToSQL(ques, context);
			}*/
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		context.setQuestions(context, question, category, subCategory);
	}

}
