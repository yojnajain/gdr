package in.yojnajain.jsonserialization;

import android.content.ContentValues;
import android.content.Context;
import in.yojnajain.gdr.DbHelper;
import in.yojnajain.model.Questions;

public class JsonToSQLiteDB extends DbHelper {

	public JsonToSQLiteDB(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public void QuesToSQL(Questions question, Context context)
	{
		ContentValues values = new ContentValues();
		values.put("question", question.getQuestion());
		values.put("option1", question.getOption1());
		values.put("option2", question.getOption2());
		values.put("option3", question.getOption3());
		values.put("option4", question.getOption4());
		values.put("option5", question.getOption5());
		values.put("correctAnswer", question.getCorrectAnswer() );
		values.put("marks", question.getMarks());
		values.put("hint", question.getHint());
		values.put("category", question.getCategory());
		values.put("subCategory", question.getSubCategory());
		values.put("serverId", question.get_id());
		DbHelper db = new DbHelper(context);
		db.insertQuestion(values, "questions");
		
		
		
	}
}
