package in.yojnajain.jsonserialization;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


import com.google.gson.Gson;

import in.yojnajain.model.User;
import android.os.AsyncTask;
import android.util.Log;

public class PostUserToURL extends AsyncTask<String, Void, Void>{
	User user;
	
	public PostUserToURL(User user) {
		super();
		this.user = user;
	}
	
	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostRequest = new HttpPost("http://gdrwebsite.herokuapp.com/api/androiduser");
			Gson gson = new Gson();
			StringEntity se = new StringEntity(gson.toJson(user));
			httpPostRequest.setEntity(se);
			Log.i("attendy json ", gson.toJson(user));
			httpPostRequest.setHeader("Accept", "application/json;charset=UTF-8");
			httpPostRequest.setHeader("Content-Type", "application/json;charset=UTF-8");
			HttpResponse httpResponse = httpClient.execute(httpPostRequest);
			Log.i("Response", EntityUtils.toString(httpResponse.getEntity()));
		} catch (ClientProtocolException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
