package in.yojnajain.model;

import java.util.Date;

public class DetailResult {

	private int id;
	private Date date;
	private String username; //Foreign key
	private String userId;
	private String emailId;
	private int score;
	private int questionNo;
	private String serverId;
	private String question;
	private String correctAnswer;
	private String userAnswer;
	private String sessionId;
	private String Category;
	private String subCategory;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private String option5;
	private int marks;
	
	
	@Override
	public String toString() {
		return "DetailResult [id=" + id + ", date=" + date + ", username="
				+ username + ", userId=" + userId + ", emailId=" + emailId
				+ ", score=" + score + ", questionNo=" + questionNo
				+ ", serverId=" + serverId + ", question=" + question
				+ ", correctAnswer=" + correctAnswer + ", userAnswer="
				+ userAnswer + ", sessionId=" + sessionId + ", Category="
				+ Category + ", subCategory=" + subCategory + ", option1="
				+ option1 + ", option2=" + option2 + ", option3=" + option3
				+ ", option4=" + option4 + ", option5=" + option5 + ", marks="
				+ marks + "]";
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public String getOption1() {
		return option1;
	}
	public void setOption1(String option1) {
		this.option1 = option1;
	}
	public String getOption2() {
		return option2;
	}
	public void setOption2(String option2) {
		this.option2 = option2;
	}
	public String getOption3() {
		return option3;
	}
	public void setOption3(String option3) {
		this.option3 = option3;
	}
	public String getOption4() {
		return option4;
	}
	public void setOption4(String option4) {
		this.option4 = option4;
	}
	public String getOption5() {
		return option5;
	}
	public void setOption5(String option5) {
		this.option5 = option5;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getQuestionNo() {
		return questionNo;
	}
	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}
	public String getServerId() {
		return serverId;
	}
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	public String getUserAnswer() {
		return userAnswer;
	}
	public void setUserAnswer(String userAnswer) {
		this.userAnswer = userAnswer;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	
	
	
}
