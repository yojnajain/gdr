package in.yojnajain.gdr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;

import in.yojnajain.jsonserialization.FetchQuestions;
import in.yojnajain.model.Questions;
import in.yojnajain.gdr.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SubCategoryListActivity extends Activity {

	ListView subCategoryList;
	String subCategory[] = { "Dealers", "Beginners", "Lower level", "Medium",
			"High" };
	ProgressDialog progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sub_category_list);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
					| ActionBar.DISPLAY_SHOW_TITLE
					| ActionBar.DISPLAY_SHOW_CUSTOM);
		}
		subCategoryList = (ListView) findViewById(R.id.listViewSubCategory);
		ArrayAdapter<String> adpt = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, subCategory);
		subCategoryList.setAdapter(adpt);

		Bundle b = this.getIntent().getExtras();
		final String category = b.getString("Category");
		final SubCategoryListActivity currentActivity = this;
		progress = new ProgressDialog(currentActivity);
		progress.setTitle("Loading");
		progress.setCanceledOnTouchOutside(false);
		subCategoryList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				progress.setMessage("Loading questions for "
						+ subCategory[position] + "...");
				progress.show();
				FetchQuestions fq = new FetchQuestions(currentActivity,
						category, subCategory[position]);
				fq.execute();

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sub_category_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {

		case R.id.user_detail:
			Intent i = new Intent(this, UserDetailActivity.class);
			startActivity(i);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	public void setQuestions(Context context, Questions[] question,
			String category, String subCategory) {
		progress.dismiss();
		if (question == null) {
			Toast.makeText(getApplicationContext(),
					"Please check your internet connection.", Toast.LENGTH_LONG)
					.show();

		} else {
			if (question.length == 0) {
				Log.i("Contesxt", context.toString());
				Toast.makeText(
						getApplicationContext(),
						"Sorry no questions available in " + subCategory
								+ " category.", Toast.LENGTH_LONG).show();

			} else {
				Intent i = new Intent(SubCategoryListActivity.this,
						QuizActivity.class);
				i.putExtra("subcategory", subCategory);
				i.putExtra("category", category);
				i.putExtra("queslist",
						new ArrayList<Questions>(Arrays.asList(question)));
				Log.i("Done", "Done");
				startActivity(i);
			}
		}
	}

}
