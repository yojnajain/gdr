package in.yojnajain.gdr;

import in.yojnajain.model.DetailResult;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import in.yojnajain.gdr.R;

public class SetDetailResultAdapter extends ArrayAdapter<DetailResult> {
	TextView question, ans;
	List<DetailResult> resultQ;
	int layoutResourceId;

	Context context;

	public SetDetailResultAdapter(Context context, List<DetailResult> result) {
		super(context, R.layout.list_detail_result, result);
		this.context = context;
		this.resultQ = result;

	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		// TODO Auto-generated method stub

		DetailResult result1 = (DetailResult) getItem(position);
		if (v == null) {
			v = LayoutInflater.from(getContext()).inflate(
					R.layout.list_detail_result, null);

		}
		question = (TextView) v.findViewById(R.id.textViewQuestion);
		ans = (TextView) v.findViewById(R.id.textViewAns);
		question.setText("Q - " + result1.getQuestionNo());
		if (result1.getUserAnswer().equals(result1.getCorrectAnswer())) {
			ans.setText("Correct");
			ans.setTextColor(Color.parseColor("#007a00"));
		} else if ((result1.getUserAnswer().equals(
				"I don't want to attempt above question.") || result1
				.getUserAnswer().equals(
						"I don't want to attempt above question"))) {

			ans.setText("Not Attempted");
			ans.setTextColor(Color.DKGRAY);
		} else {
			ans.setText("Incorrect");
			ans.setTextColor(Color.RED);

		}
		return v;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle saveInstanceState) {
		return inflater.inflate(R.layout.list_detail_result, container, false);
	}

	public void onViewCreated(View view, Bundle savedInstanceState) {

	}

}
