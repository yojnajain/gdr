package in.yojnajain.gdr;

import in.yojnajain.gdr.R;
import android.os.Build;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class CategoryListActivity extends Activity {

	String category[] = { "Mutual Fund", "Capital Market", "Securities Market" };
	ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_list);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
					| ActionBar.DISPLAY_SHOW_TITLE
					| ActionBar.DISPLAY_SHOW_CUSTOM);
		}
		lv = (ListView) findViewById(R.id.listViewCategory);
		// DbHelper db=new DbHelper(getApplicationContext());
		SharedPreferences prefs = this.getSharedPreferences("in.yojnajain.gdr",
				Context.MODE_PRIVATE);
		if (!prefs.getBoolean("testDataLoaded", false)) {
			// db.addData();
			// prefs.edit().putBoolean("testDataLoaded", true).commit();
		}

		ArrayAdapter<String> adpt = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, category);
		lv.setAdapter(adpt);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent i = new Intent(CategoryListActivity.this,
						SubCategoryListActivity.class);
				Log.i("Selected category", category[position]);
				i.putExtra("Category", category[position]);
				startActivity(i);
			}

		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.category_list, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {

		case R.id.user_detail:
			Intent i = new Intent(this, UserDetailActivity.class);
			startActivity(i);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

}
