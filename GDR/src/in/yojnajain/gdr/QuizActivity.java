package in.yojnajain.gdr;
// IMPORTING PACKAGES
import in.yojnajain.model.Questions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
//CHECk what is going on

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends Activity implements OnClickListener {
	List<Questions> quesList;
	int score = 0, totalMarks = 0;
	int qid, count = 0, totalQuestions;
	String sessionId;
	Questions currentQ;
	TextView txtQuestion, questionNo, txtmarks;
	RadioButton rd1, rd2, rd3, rd4, rd5;
	RadioGroup grp;
	Button buttonNext, buttonHint, buttonScore;
	Button alertDialog;
	AlertDialog.Builder alert;
	public static MediaPlayer mp, a, up, correct, levelfinished, startsound,
			gamefinished;
	String category, subcategory;
	Bundle instance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		Bundle extras = getIntent().getExtras();

		category = extras.getString("category");
		subcategory = extras.getString("subcategory");
		quesList = (ArrayList<Questions>) extras.get("queslist");
		totalQuestions = quesList.size();
		Random r = new Random();
		qid = r.nextInt(quesList.size() - 1);

		if (savedInstanceState != null) {
			qid = savedInstanceState.getInt("Question Number");
			score = savedInstanceState.getInt("Score");
			totalMarks = savedInstanceState.getInt("TotalMarks");

		}
		SharedPreferences pref = getSharedPreferences("mypreferences",
				MODE_PRIVATE);
		if (pref.contains("sessionId")) {

			sessionId = pref.getString("sessionId", "");
		} else {

			sessionId = UUID.randomUUID().toString();
		}
		/*
		 * up = MediaPlayer.create(this, R.raw.up); a = MediaPlayer.create(this,
		 * R.raw.a); startsound = MediaPlayer.create(this, R.raw.kbc_start);
		 * correct = MediaPlayer.create(this, R.raw.clap2); levelfinished =
		 * MediaPlayer.create(this, R.raw.clap3); gamefinished =
		 * MediaPlayer.create(this, R.raw.clap4);
		 * 
		 * startsound.start();
		 */
		questionNo = (TextView) findViewById(R.id.textViewQuestionNo);
		txtQuestion = (TextView) findViewById(R.id.textViewQuestion);
		txtmarks = (TextView) findViewById(R.id.textViewMarks);
		rd1 = (RadioButton) findViewById(R.id.radio1);
		rd2 = (RadioButton) findViewById(R.id.radio2);
		rd3 = (RadioButton) findViewById(R.id.radio3);
		rd4 = (RadioButton) findViewById(R.id.radio4);
		rd5 = (RadioButton) findViewById(R.id.radio5);
		grp = (RadioGroup) findViewById(R.id.radioGroup1);
		buttonNext = (Button) findViewById(R.id.buttonNext);
		buttonHint = (Button) findViewById(R.id.buttonHint);
		buttonScore = (Button) findViewById(R.id.buttonScore);
		

		if (quesList.size() > 0) {

			currentQ = quesList.get(qid);
			setQuestionView();

		} else {
			Toast.makeText(getApplicationContext(),
					"Sorry No questions available", Toast.LENGTH_LONG).show();

		}

		// alert = new AlertDialog.Builder(this);
		// alert.setTitle(" Confirmation ");
		// alert.setMessage("Are you sure want to lock this answer?");

		/*
		 * alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		 * 
		 * @Override public void onClick(DialogInterface dialog, int which) { //
		 * TODO Auto-generated method stub
		 * 
		 * grp = (RadioGroup) findViewById(R.id.radioGroup1); if
		 * (grp.getCheckedRadioButtonId() != 0) { Log.i("2", "2"); RadioButton
		 * answer = (RadioButton) findViewById(grp .getCheckedRadioButtonId());
		 * Log.i("yourans", currentQ.getCorrectAnswer() + " " +
		 * answer.getText());
		 * 
		 * if (currentQ.getCorrectAnswer().equals(answer.getText())) { score++;
		 * 
		 * Log.d("score", "Your score" + score +"  "+totalMarks ); } }
		 * 
		 * 
		 * if (qid == quesList.size() - 1) { buttonNext.setText("Finish");
		 * 
		 * } if (qid < quesList.size()) { currentQ = quesList.get(qid);
		 * setQuestionView();
		 * 
		 * } else { Intent intent = new Intent(QuizActivity.this,
		 * ResultActivity.class); Bundle b = new Bundle(); b.putInt("score",
		 * score); // Your score // Put your score to your next Intent
		 * b.putString("category", category); b.putString("subcategory",
		 * subcategory); b.putString("totalmarks", ""+totalMarks);
		 * intent.putExtras(b); startActivity(intent);
		 * 
		 * }
		 * 
		 * } });
		 * 
		 * alert.setNegativeButton("Cancel", new
		 * DialogInterface.OnClickListener() {
		 * 
		 * @Override public void onClick(DialogInterface dialog, int which) { //
		 * TODO Auto-generated method stub
		 * 
		 * } });
		 */
		
		buttonHint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (currentQ.getHint().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Hint is not available", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), currentQ.getHint(),
							Toast.LENGTH_LONG).show();
				}
			}
		});
		/*
		 * buttonSubmit.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub
		 * 
		 * } });
		 */
		buttonScore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(QuizActivity.this,
						ResultActivity.class);
				Bundle b = new Bundle();
				b.putInt("score", score); // Your score
				b.putString("category", category);
				b.putString("subcategory", subcategory);
				b.putString("totalmarks", "" + totalMarks);
				b.putString("sessionId", sessionId);
				intent.putExtras(b); // Put your score to your next Intent
				startActivity(intent);
			}
		});
		buttonNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (grp.getCheckedRadioButtonId() != -1) {

					RadioButton answer = (RadioButton) findViewById(grp
							.getCheckedRadioButtonId());
					if (answer.getText() == null) {

					} else {
						checkCorrectAns();
						grp.clearCheck();

					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Please select answer", Toast.LENGTH_LONG).show();

				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_quiz, menu);
		return true;
	}

	private void setQuestionView() {
		count++;
		rd3.setEnabled(true);
		rd4.setEnabled(true);
		rd5.setEnabled(true);
		rd3.setVisibility(0);
		rd4.setVisibility(0);
		rd5.setVisibility(0);
		if (quesList.size() == 1) {
			buttonNext.setText("Finish");
		}
		if (qid >= 0) {
			buttonScore.setEnabled(true);
		} else {
			buttonScore.setEnabled(false);
		}
		Animation animationToRight = new TranslateAnimation(-400, 10, 0, 0);
		animationToRight.setDuration(500);
		animationToRight.setRepeatMode(Animation.ABSOLUTE);
		// animationToRight.setRepeatCount(Animation.INFINITE);

		txtQuestion.setAnimation(animationToRight);
		int a = count;
		questionNo.setText("Question No : " + a + "/" + totalQuestions);
		txtmarks.setText("Marks:" + currentQ.getMarks());
		txtQuestion.setText(currentQ.getQuestion());
		rd1.setText(currentQ.getOption1());
		rd2.setText(currentQ.getOption2());
		rd3.setText(currentQ.getOption3());
		rd4.setText(currentQ.getOption4());
		rd5.setText(currentQ.getOption5());
		if (currentQ.getOption3().equals("")) {
			rd3.setEnabled(false);
			rd3.setVisibility(View.GONE);

		}
		if (currentQ.getOption4().equals("")) {
			rd4.setEnabled(false);
			rd4.setVisibility(View.GONE);

		}
		if (currentQ.getOption5().equals("")) {
			rd5.setEnabled(false);
			rd5.setVisibility(View.GONE);

		}
		
		
	}

	public void checkCorrectAns() {

		grp = (RadioGroup) findViewById(R.id.radioGroup1);
		if (grp.getCheckedRadioButtonId() != 0) {

			RadioButton answer = (RadioButton) findViewById(grp
					.getCheckedRadioButtonId());
			// Log.i("yourans",currentQ.getCorrectAnswer() + " " +
			// answer.getText());

			if (currentQ.getCorrectAnswer().equals(answer.getText())) {
				score += currentQ.getMarks();
				totalMarks = totalMarks + currentQ.getMarks();
			} else if (answer.getText().equals(
					"I don't want to attempt above question.")
					|| answer.getText().equals(
							"I don't want to attempt above question")) {
			} else {
				totalMarks = totalMarks + currentQ.getMarks();
			}
			// Log.i("score", "Your score" + score + "  " + totalMarks);
		}

		setDetailResult();
		if (quesList.size() > 0) {

			Random r = new Random();
			quesList.remove(qid);
			Log.i("size", "" + quesList.size());
			if (quesList.size() == 1) {
				qid = 0;
				currentQ = quesList.get(qid);
				setQuestionView();
			} else if (quesList.size() > 1) {
				qid = r.nextInt(quesList.size() - 1);
				currentQ = quesList.get(qid);
				setQuestionView();
			} else {
				Intent intent = new Intent(QuizActivity.this,
						ResultActivity.class);
				Bundle b = new Bundle();
				b.putInt("score", score); // Your score
				// Put your score to your next Intent
				b.putString("category", category);
				b.putString("subcategory", subcategory);
				b.putString("totalmarks", "" + totalMarks);
				b.putString("sessionId", sessionId);
				intent.putExtras(b);

				startActivity(intent);
			}

		} else {
			Intent intent = new Intent(QuizActivity.this, ResultActivity.class);
			Bundle b = new Bundle();
			b.putInt("score", score); // Your score
			// Put your score to your next Intent
			b.putString("category", category);
			b.putString("subcategory", subcategory);
			b.putString("totalmarks", "" + totalMarks);
			b.putString("sessionId", sessionId);
			intent.putExtras(b);

			startActivity(intent);

		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putInt("Question Number", count + 1);
		outState.putInt("Score", score);
		outState.putInt("TotalMarks", totalMarks);
		outState.putString("sessionId", sessionId);

	}

	private ContentValues setDetailResult() {
		ContentValues c = new ContentValues();
		c.put("category", currentQ.getCategory());
		c.put("subCategory", currentQ.getSubCategory());
		c.put("question", currentQ.getQuestion());
		c.put("correctAnswer", currentQ.getCorrectAnswer());
		c.put("questionNo", count);
		RadioButton answer = (RadioButton) findViewById(grp
				.getCheckedRadioButtonId());
		c.put("userAnswer", answer.getText().toString());

		c.put("emailId", "");
		c.put("username", "");
		c.put("userId", "");
		c.put("serverId", currentQ.get_id());
		c.put("date", "" + new Date());
		c.put("sessionId", sessionId.toString());
		c.put("option1", currentQ.getOption1());
		c.put("option2", currentQ.getOption2());
		c.put("option3", currentQ.getOption3());
		c.put("option4", currentQ.getOption4());
		c.put("option5", currentQ.getOption5());
		c.put("marks", currentQ.getMarks());
		DbHelper db = new DbHelper(getApplicationContext());
		db.insertAttendedQuestion(c);
		return c;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent i = new Intent(QuizActivity.this,
					CategoryListActivity.class);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
