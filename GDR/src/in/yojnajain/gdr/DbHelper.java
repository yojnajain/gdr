package in.yojnajain.gdr;

import in.yojnajain.jsonserialization.PostUserToURL;
import in.yojnajain.model.DetailResult;
import in.yojnajain.model.Questions;
import in.yojnajain.model.User;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "GDRdb";

	private SQLiteDatabase dbase;

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		final String create_questions_table = "CREATE TABLE IF NOT EXISTS questions ( _id INTEGER PRIMAY KEY "
				+ ", question TEXT, option1 TEXT, option2 TEXT, option3 TEXT, option4 TEXT, option5 TEXT, correctAnswer TEXT"
				+ " , hint TEXT, category TEXT, subCategory TEXT, marks NUMERIC, serverId TEXT)";
		db.execSQL(create_questions_table);

		// final String create_category_table =
		// "CREATE TABLE IF NOT EXISTS category ( _id INTEGER PRIMARY KEY "
		// +", categoryName TEXT )";
		// db.execSQL(create_category_table);

		final String create_detail_result_table = "CREATE TABLE IF NOT EXISTS detailResult (_id INTEGER PRIMARY KEY, username TEXT, userId TEXT, category TEXT"
				+ ", subCategory TEXT, question TEXT, correctAnswer TEXT, userAnswer TEXT, emailId TEXT, score NUMERIC, sessionId TEXT, serverId TEXT, date Date, questionNo NUMERIC"
				+", option1 TEXT, option2 TEXT, option3 TEXT, option4 TEXT, option5 TEXT, marks NUMERIC)";
		db.execSQL(create_detail_result_table);
		final String create_user_table = "CREATE TABLE IF NOT EXISTS user ( _id INTEGER PRIMARY KEY "
				+ ", email TEXT, name TEXT, age NUMERIC, gender TEXT, city TEXT, mobileNo TEXT, qualification TEXT )";
		db.execSQL(create_user_table);

		/*
		 * final String create_result_table =
		 * "CREATE TABLE IF NOT EXISTS result ( _id INTEGER PRIMARY KEY "
		 * +", date DATE, userName TEXT, email TEXT, score TEXT, category TEXT )"
		 * ; // db.execSQL(create_result_table);
		 */
		Log.i("databse created", "");

		// addData();

	}

	public void insertAttendedQuestion(ContentValues c) {
		dbase = this.getWritableDatabase();
		dbase.insert("detailResult", null, c);
		dbase.close();
	}

	public void addData() {
		dbase = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		/*
		 * // Category Table values.put("categoryName", "Mutual Fund");
		 * dbase.insert("category", null, values); values.clear();
		 * 
		 * values.put("categoryName", "Capital Market");
		 * dbase.insert("category", null, values); values.clear();
		 * 
		 * values.put("categoryName", "Securities Market");
		 * dbase.insert("category", null, values); values.clear();
		 */
		// Sub Category

		// Questions Table

		values.put(
				"question",
				"Appeal against the orders Securities and Exchange Board of India can be made to ________ .");
		values.put("option1", "Central Government");
		values.put("option2", "Securities Appllate Tribunal");
		values.put("option3", "Registrar of Companies");
		values.put("option4", "High Court");
		values.put("option5", "");
		values.put("correctAnswer", "Securities Appllate Tribunal");
		values.put("marks", 2);
		values.put("hint", "");
		values.put("category", "Capital Market");
		values.put("subCategory", "Dealers");
		dbase.insert("questions", null, values);
		values.clear();

		values.put("question", "In ASBA, the amount is blocked in");
		values.put("option1", "Trading members account");
		values.put("option2", "Investors own account");
		values.put("option3", "Both A and B");
		values.put("option4", "None of the above");
		values.put("option5", "");
		values.put("correctAnswer", "Investors own account");
		values.put("marks", 2);
		values.put("hint", "");
		values.put("category", "Capital Market");
		values.put("subCategory", "Dealers");
		dbase.insert("questions", null, values);
		values.clear();

		values.put("question",
				"The Capital Market Segment of NSE commenced operations in");
		values.put("option1", "Sep-96");
		values.put("option2", "Nov-94");
		values.put("option3", "Aug-96");
		values.put("option4", "Nov-96");
		values.put("option5", "");
		values.put("correctAnswer", "Nov-94");
		values.put("marks", 2);
		values.put("hint", "");
		values.put("category", "Capital Market");
		values.put("subCategory", "Dealers");
		dbase.insert("questions", null, values);
		values.clear();

		values.put(
				"question",
				"Appeal against the orders Securities and Exchange Board of India can be made to ________ .");
		values.put("option1", "Central Government");
		values.put("option2", "Securities Appllate Tribunal");
		values.put("option3", "Registrar of Companies");
		values.put("option4", "High Court");
		values.put("option5", "");
		values.put("correctAnswer", "Securities Appllate Tribunal");
		values.put("marks", 2);
		values.put("hint", "");
		values.put("category", "Capital Market");
		values.put("subCategory", "Dealers");
		dbase.insert("questions", null, values);
		values.clear();

		values.put("question",
				"The first two characters in ISIN code for a security represents _____.");
		values.put("option1", "issuer type");
		values.put("option2", "security type");
		values.put("option3", "country code");
		values.put("option4", "company identity");
		values.put("option5", "");
		values.put("correctAnswer", "country code");
		values.put("marks", 2);
		values.put("hint", "");
		values.put("category", "Capital Market");
		values.put("subCategory", "Dealers");
		dbase.insert("questions", null, values);
		values.clear();

		values.put("question",
				"Who provides counter party guarantee for all trades executed on NSEIL?");
		values.put("option1", "NSE Clearing House");
		values.put("option2", "NSEIL");
		values.put("option3", "NSDL");
		values.put("option4", "NSCCL");
		values.put("option5", "");
		values.put("correctAnswer", "NSCCL");
		values.put("marks", 2);
		values.put("hint", "");
		values.put("category", "Capital Market");
		values.put("subCategory", "Dealers");
		dbase.insert("questions", null, values);

		Log.i("Insersion Successful", "Success");
		dbase.close();
	}

	public List<DetailResult> getSessionQuestions(String sessionId) {
		List<DetailResult> list = new ArrayList<DetailResult>();
		dbase = this.getReadableDatabase();
		String query = "SELECT * from detailResult WHERE sessionId = '"+ sessionId+"'";
		Cursor cursor = dbase.rawQuery(query, null);
        	
		if (cursor.moveToFirst() && cursor != null) {

			do {

				DetailResult rs = new DetailResult();
				rs.setId(cursor.getInt(0));
				rs.setUsername(cursor.getString(1));
				rs.setUserId(cursor.getString(2));
				rs.setCategory(cursor.getString(3));
				rs.setSubCategory(cursor.getString(4));
				rs.setQuestion(cursor.getString(5));
				rs.setCorrectAnswer(cursor.getString(6));
				rs.setUserAnswer(cursor.getString(7));
				rs.setEmailId(cursor.getString(8));
				rs.setScore(cursor.getInt(9));
				rs.setSessionId(cursor.getString(10));
				rs.setServerId(cursor.getString(11));
				rs.setQuestionNo(cursor.getInt(13));
				
				rs.setOption1(cursor.getString(14));
				rs.setOption2(cursor.getString(15));
				rs.setOption3(cursor.getString(16));
				rs.setOption4(cursor.getString(17));
				rs.setOption5(cursor.getString(18));
				rs.setMarks(cursor.getInt(19));
				/*
				 * SimpleDateFormat sdfFull = new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 * 
				 * try { //rs.setDate(sdfFull.parse(cursor.getString(12))); }
				 * catch (ParseException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); }
				 */
				list.add(rs);
			} while (cursor.moveToNext());

		}
		dbase.close();	
		return list;

	}

	public void addUserDetail(ContentValues v) {
		dbase = this.getWritableDatabase();
		dbase.insert("user", null, v);
		Log.i("User Data Successfully save", "done");
		dbase.close();
		User user = new User();
		user.setAge(v.getAsString("age"));
		user.setGender(v.getAsString("gender"));
		user.setCity(v.getAsString("city"));
		user.setEmail(v.getAsString("email"));
		user.setName(v.getAsString("name"));
		user.setMobileNo(v.getAsString("mobileNo"));
		user.setQualification(v.getAsString("qualification"));
		PostUserToURL putu = new PostUserToURL(user);
		putu.execute();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
		// Drop older table if existed

	}

	public List<Questions> getAllQuestions() {
		List<Questions> quesList = new ArrayList<Questions>();
		// Select All Query
		String selectQuery = "SELECT * FROM questions";
		dbase = this.getReadableDatabase();
		Cursor cursor = dbase.rawQuery(selectQuery, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Questions quest = new Questions();
				quest.setId(cursor.getInt(0));
				quest.setQuestion(cursor.getString(1));
				quest.setCorrectAnswer(cursor.getString(7));
				quest.setOption1(cursor.getString(2));
				quest.setOption2(cursor.getString(3));
				quest.setOption3(cursor.getString(4));
				quest.setOption4(cursor.getString(5));
				quest.setOption5(cursor.getString(6));
				quest.setHint(cursor.getString(8));
				quest.setCategory(cursor.getString(9));
				quest.setSubCategory(cursor.getString(10));
				quest.setMarks(cursor.getInt(11));
				quesList.add(quest);

			} while (cursor.moveToNext());
		}
		dbase.close();
		// return quest list
		return quesList;
	}

	public void insertQuestion(ContentValues v, String tablename) {

		dbase = this.getWritableDatabase();
		dbase.insert(tablename, null, v);
		dbase.close();

	}
}
