
package in.yojnajain.gdr;

import in.yojnajain.gdr.R;
import in.yojnajain.gdr.R.layout;
import in.yojnajain.gdr.R.menu;


import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class UserDetailActivity extends Activity {

	EditText edEmail,edName,edAge,edCity,edMob,edQua;
	String email,name,age,gender,city,mob,qua;
	Button btnSubmit;
	SQLiteDatabase dbase;
	RadioGroup rdGroup;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_detail);
		edEmail=(EditText)findViewById(R.id.editText1);
		edName=(EditText)findViewById(R.id.editText2);
		edAge=(EditText)findViewById(R.id.editText3);
		edCity=(EditText)findViewById(R.id.editText5);
		edMob=(EditText)findViewById(R.id.editText6);
		edQua=(EditText)findViewById(R.id.editText7);
		rdGroup=(RadioGroup)findViewById(R.id.radioGroup1);
		btnSubmit=(Button)findViewById(R.id.button1);
		btnSubmit.setOnClickListener(new View.OnClickListener() {			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				email=edEmail.getText().toString();
				name=edName.getText().toString();
				age=edAge.getText().toString();
				city=edCity.getText().toString();
				mob=edMob.getText().toString();
				qua=edQua.getText().toString();
				
				if(rdGroup.getCheckedRadioButtonId()==1)
				{
					gender = "male" ;
				}
				else
				{
					gender = "female" ;
				}
				
				ContentValues values = new ContentValues();
				values.put("email", email);
				values.put("name", name);
				values.put("age", age);
				values.put("gender", gender);
				values.put("city", city);
				values.put("mobileNo",mob);
				values.put("qualification", qua);
				DbHelper dbhelper=new DbHelper(getApplicationContext());
				dbhelper.addUserDetail(values);
				
				
				
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_detail, menu);
		return true;
	}

}
