package in.yojnajain.gdr;

import in.yojnajain.model.DetailResult;

import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;

public class ResultActivity extends Activity {
	TextView txtAct, txtSubact, txtScore;
	String totalmarks, sessionId;
	SetDetailResultAdapter custAd1;
	ListView lv;
	ImageButton app_icon;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		SharedPreferences preferences = getSharedPreferences("mypreferences", 0);
		preferences.edit().remove("sessionId").commit();
		txtAct = (TextView) findViewById(R.id.txtAct);
		txtSubact = (TextView) findViewById(R.id.txtSubact);
		txtScore = (TextView) findViewById(R.id.txtScore);
		RatingBar bar = (RatingBar) findViewById(R.id.ratingBar1);

		bar.setNumStars(5);
		bar.setStepSize(0.5f);
		// get text view

		TextView t = (TextView) findViewById(R.id.textResult);
		// get score
		Bundle b = getIntent().getExtras();
		int score = b.getInt("score");
		totalmarks = b.getString("totalmarks");
		String category = b.getString("category");
		String subcategory = b.getString("subcategory");
		sessionId = b.getString("sessionId");
		txtScore.setText("Your score = " + score + "/" + totalmarks);
		txtAct.setText("Subject -> " + category);
		txtSubact.setText("Level -> " + subcategory);

		// display score

		int sc = (int) ((5.0 * score) / Float.parseFloat(totalmarks));

		bar.setRating(sc);
		bar.setEnabled(false);
		switch (sc) {
		case 0:
			t.setText("Oopsie! Better Luck Next Time!");
			break;
		case 1:
			t.setText("Try again for best score.");
		case 2:
		case 3:
			t.setText("Good");
			break;
		case 4:
			t.setText("Very Good");
			break;
		case 5:
			t.setText("Excellent");
			break;
		}

		lv = (ListView) findViewById(R.id.listQuestions);
		DbHelper db = new DbHelper(getApplicationContext());
		final List<DetailResult> lt = db.getSessionQuestions(sessionId);
		Log.i("Count", "" + lt.size());
		custAd1 = new SetDetailResultAdapter(this, lt);
		lv.setAdapter(custAd1);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ResultActivity.this, WatchQuestion.class);
				Bundle b = new Bundle();
				Gson g = new Gson();
				b.putString("question", g.toJson(lt.get(position)));
				i.putExtras(b);
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent i = new Intent(ResultActivity.this,
					CategoryListActivity.class);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
