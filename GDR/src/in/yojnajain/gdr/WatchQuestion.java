package in.yojnajain.gdr;

import in.yojnajain.model.DetailResult;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;

public class WatchQuestion extends Activity {

	DetailResult currentQ;
	TextView txtQuestion, questionNo, txtmarks;
	RadioButton rd1, rd2, rd3, rd4, rd5;
	RadioGroup grp;
	Button buttonNext, buttonHint, buttonScore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		Bundle b = getIntent().getExtras();
		Gson gson = new Gson();
		currentQ = gson.fromJson(b.getString("question"), DetailResult.class);
		questionNo = (TextView) findViewById(R.id.textViewQuestionNo);
		txtQuestion = (TextView) findViewById(R.id.textViewQuestion);
		txtmarks = (TextView) findViewById(R.id.textViewMarks);
		rd1 = (RadioButton) findViewById(R.id.radio1);
		rd2 = (RadioButton) findViewById(R.id.radio2);
		rd3 = (RadioButton) findViewById(R.id.radio3);
		rd4 = (RadioButton) findViewById(R.id.radio4);
		rd5 = (RadioButton) findViewById(R.id.radio5);
		grp = (RadioGroup) findViewById(R.id.radioGroup1);
		buttonNext = (Button) findViewById(R.id.buttonNext);
		buttonHint = (Button) findViewById(R.id.buttonHint);
		buttonScore = (Button) findViewById(R.id.buttonScore);

		buttonNext.setVisibility(View.GONE);
		buttonScore.setVisibility(View.GONE);
		buttonHint.setVisibility(View.GONE);
		setQuestionView();

		rd1.setEnabled(false);
		rd2.setEnabled(false);
		rd3.setEnabled(false);
		rd4.setEnabled(false);
		rd5.setEnabled(false);
	}

	private void setQuestionView() {

		rd3.setVisibility(0);
		rd4.setVisibility(0);
		rd5.setVisibility(0);

		questionNo.setText("Question No : " + currentQ.getQuestionNo());
		txtmarks.setText("Marks:" + currentQ.getMarks());
		txtQuestion.setText(currentQ.getQuestion());
		rd1.setText(currentQ.getOption1());
		rd2.setText(currentQ.getOption2());
		rd3.setText(currentQ.getOption3());
		rd4.setText(currentQ.getOption4());
		rd5.setText(currentQ.getOption5());
		if (currentQ.getOption5().equals("")) {
			rd5.setVisibility(View.GONE);
		}
		if (currentQ.getOption4().equals("")) {
			rd4.setVisibility(View.GONE);
		}
		if (currentQ.getOption3().equals("")) {
			rd3.setVisibility(View.GONE);
		}

		findWrongAnswer();
		findCorrectAns();

	}

	private void findCorrectAns() {
		if (currentQ.getOption1().equals(currentQ.getCorrectAnswer())) {
			rd1.setTextColor(Color.parseColor("#007a00"));
		} else if (currentQ.getOption2().equals(currentQ.getCorrectAnswer())) {
			rd2.setTextColor(Color.parseColor("#007a00"));
		} else if (currentQ.getOption3().equals(currentQ.getCorrectAnswer())) {
			rd3.setTextColor(Color.parseColor("#007a00"));
		} else if (currentQ.getOption4().equals(currentQ.getCorrectAnswer())) {
			rd4.setTextColor(Color.parseColor("#007a00"));
		} else if (currentQ.getOption5().equals(currentQ.getCorrectAnswer())) {
			rd5.setTextColor(Color.parseColor("#007a00"));
		}

	}

	private void findWrongAnswer() {
		if (currentQ.getOption1().equals(currentQ.getUserAnswer())) {
			rd1.setTextColor(Color.RED);
		} else if (currentQ.getOption2().equals(currentQ.getUserAnswer())) {
			rd2.setTextColor(Color.RED);
		} else if (currentQ.getOption2().equals(currentQ.getUserAnswer())) {
			rd2.setTextColor(Color.RED);
		} else if (currentQ.getOption2().equals(currentQ.getUserAnswer())) {
			rd2.setTextColor(Color.RED);
		} else if (currentQ.getOption2().equals(currentQ.getUserAnswer())) {
			rd2.setTextColor(Color.RED);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent i = new Intent(WatchQuestion.this,
					CategoryListActivity.class);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
